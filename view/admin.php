<h2>Gestion Personne</h2>
<div class="border">
    <fieldset>
        <legend>Ajouter une personne</legend>
        <form action="?page=action_add_person" method="POST">
            <input type="text" placeholder="Nom" name="lastname" id="">
            <input type="text" placeholder="Prénom" name="firstname" id="">
            <input type="submit" value="Ajouter">
        </form>
    </fieldset>
    <fieldset>
        <legend>Modifier personne</legend>
        <form action="?page=action_update_people" enctype="multipart/form-data" method="POST">
            <select name="people_update_id" id="people_update_id">
                <option value="0">Choisissez une personne</option>
                <?php 
                    foreach ($persons as $key => $person) {
                        echo "<option value='{$person->getId()}'>{$person->getFirstname()} {$person->getLastname()}</option>";
                    }
                ?>
            </select>
            <input type="text" name="lastname" id="updateLastname">
            <input type="text" name="firstname" id="updateFirstname">

            <input type="submit" value="Modifier">

        </form>

    </fieldset>
    <fieldset>
        <legend>Gestion Image</legend>
        <form action="?page=action_update_people_image" enctype="multipart/form-data" method="POST">
            <select name="people_update_id" id="people_update_id">
                <option value="0">Choisissez une personne</option>
                <?php 
                    foreach ($persons as $key => $person) {
                        echo "<option value='{$person->getId()}'>{$person->getFirstname()} {$person->getLastname()}</option>";
                    }
                ?>
            </select>
            <input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
            <input type="file" name="people_image" accept=".jpg, image/png" id="">
            <input type="submit" value="Envoyer">
        </form>
    </fieldset>
</div>

<h2>Gestion Numéro</h2>
<div class="border">
    <fieldset>
        <legend>Ajouter un numéro</legend>

        <form action="?page=action_add_number" method="POST">
            <select name="number_add_people_id" id="number_add_people_id">
                <option value="0">Choisissez une personne</option>
                <?php 
                    foreach ($persons as $key => $person) {
                        echo "
                        <option value='{$person->getId()}'>
                            {$person->getFirstname()} {$person->getLastname()}
                        </option>";
                    }
                ?>
            </select>
            <input type="text" name="number_add_number" id="">
            <select name="number_add_type_id" id="number_add_type_id">
                <option value="0">Choisissez un type</option>

            </select>
            <input type="submit" value="Ajouter">
        </form>
    </fieldset>

    <fieldset>
        <legend>Supprimer un numéro</legend>

        <form action="?page=action_delete_number" method="POST">
            <select name="number_del_number_id" id="">

            </select>

            <input type="submit" value="Supprimer">
        </form>
    </fieldset>
</div>