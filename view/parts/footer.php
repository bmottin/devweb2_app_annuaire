</div>
<footer>
        <ul class="nav justify-content-center navbar-dark bg-primary mt-4">

            <li class="nav-item ">
                <a class="nav-link text-light" href="https://fim.fr">&copy; FIM 2023 - By FIM IT TEAM</a>
            </li>
        </ul>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"
        defer></script>
</body>

</html>