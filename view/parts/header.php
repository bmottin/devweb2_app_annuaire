<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mon Annuaire - </title>

    <link rel="stylesheet" href="./public/css/bootstrap.skectchy.min.css">
    <link rel="stylesheet" href="./public/css/style.css">

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="?page=home">Mon Annuaire </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
            aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="?page=home">Home</a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="?page=proceed_logout">Se déconnecter</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=admin">Administrer</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="?page=login">Se connecter</a>
                </li>

            </ul>
        </div>
    </nav>
    <div class="container mt-1 main-content">