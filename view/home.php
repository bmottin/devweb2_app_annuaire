<?php


echo "<div class='row'>";

foreach ($persons as $key => $person) {
    echo "
    <div class='card col-12 col-md-6 col-lg-3 m-0'>
        <div class='card-body'>";
    if ($person->getAvatar()) {
        // si j'ai à une image
        echo "<img src='./public/img/{$person->getAvatar()}' class='card-img-top rounded-circle border' alt=''>";
    } else {
        // sinon celle par défaut
        echo "<img src='./public/img/silhouette_sayan.png' class='card-img-top rounded-circle border' alt=''>";
    }
    echo "<h4 class='card-title'>
            {$person->getFirstname()} {$person->getLastname()} 
            </h4>
            <div class='card-text'>
                <ul>";
    if ($person->getNumbers()) {
        foreach ($person->getNumbers() as $key => $number) {
            if ($number->getNumType()->getLabel() == "pro") {
                echo "<li>📳{$number->getPhoneNumber()}</li>";
            } else {
                echo "<li>📞{$number->getPhoneNumber()}</li>";
            }
        }
    } else {
        echo "Pas de numéro";
    }
    echo "</ul>
            </div>
        </div>
    </div>";
}

echo "</div>";
