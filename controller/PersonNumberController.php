<?php

class PersonNumberController
{

    function getNumbersByPersonId($id)
    {
        $req = "SELECT * FROM `numbers` WHERE people_id=:id";

        $dataArgs = [
            ":id" => $id
        ];

        $numbers = databaseRead($req, $dataArgs);
        $ntc = new NumTypeController();
        $personNumbers = [];
        foreach ($numbers as $key => $num) {
            $number = new PersonNumber();
            $number->setId($num['id']);
            $number->setPhoneNumber($num['number']);
            $number->setNumType($ntc->getNumTypeById($num['number_type_id']));
            $personNumbers[] = $number;
        }

        return $personNumbers;
    }
}
