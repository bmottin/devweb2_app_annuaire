<?php

function databaseConnect()
{
    $server = "localhost";
    $port = "3306";
    $dbname = "devweb_app_annuaire_230202";
    $user = "root";
    $password = "";

    $pdo = new PDO("mysql:host={$server};port={$port};dbname={$dbname};", $user, $password);

    // parametre Mysql pour afficher les erreurs
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec("set names utf8");

    return $pdo;
}

function databaseRead($req, $dataArgs=[], $isUnique=false)
{
    // je recupere la connction à la BDD
    $pdo = databaseConnect();

    // prepare la requête, 
    // c-a-d on sécurise la requete pour évité les injection
    $stmt = $pdo->prepare($req);
    // on l'execute
    $stmt->execute($dataArgs);

    if ($isUnique) {
        // je recupere une seule ligne
        $rows = $stmt->fetch();
    } else {
        // isUnique est faux
        // je recupere une seule ligne
        // je recupere tous les enregistrements sous forme de tableau associatif
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    // on renvoie les lignes trouvées
    return $rows;
}

function databaseInsert($req, $dataArgs=[]){
 
    $pdo = databaseConnect();

    $stmt = $pdo->prepare($req);
    $stmt->execute($dataArgs);

    
    
}

