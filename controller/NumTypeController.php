<?php

class NumTypeController
{

    function getNumTypes()
    {
        // Envoie une liste d'instance de NumType

        $req = "SELECT * FROM `number_types`;";

        $numTypes = databaseRead($req);

        $types = [];
        // pour chaque ligne SQL
        foreach ($numTypes as $key => $nType) {
            // je creer une nouvelle instance de NumType
            // je que je stock dans mon tableau Types
            $types[] = new NumType($nType["id"], $nType["label"]);
        }

        // je renvoie mon tableau contenu mes instances de NumType
        return $types;
        // return [
        //     new NumType(1, "Pro"),
        //     new NumType(2, "Perso")
        // ];
    }

    function getNumTypeById($id)
    {
        // 
        $req = "SELECT * FROM `number_types` WHERE id=:id limit 1;";

        // tableau associatif de parametre
        $dataArgs = [
            ":id" => $id
        ];
        // j'envoie à Read ma requete, le tableau de param, et le bool pour une seule ligne
        $numType = databaseRead($req, $dataArgs, true);
        
        // je creer une nouvelle instance de NumType
        return new NumType($numType["id"], $numType["label"]);
    }
}
