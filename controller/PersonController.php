<?php

class PersonController
{

    function getPersons()
    {
        $persons = [];
        $req = "SELECT * FROM `peoples`";
        $pers = databaseRead($req);

        $pnc = new PersonNumberController();

        // pour chaque ligne SQL
        foreach ($pers as $key => $p) {

            $numbers = $pnc->getNumbersByPersonId($p['id']);
            // je creer une nouvelle instance de NumType
            // je que je stock dans mon tableau Types
            $person = new Person();
            $person->setId($p["id"]);
            $person->setLastname($p['lastname']);
            $person->setFirstname($p['firstname']);
            $person->setAvatar($p['image']);
            // FIXME add user
            // $person->setNumbers($numbers);
            foreach ($numbers as $key => $number) {
                $person->addNumbers($number);
            }
            $persons[] = $person;
        }

        // je renvoie mon tableau contenu mes instances de NumType
        return $persons;
    }

    function addPerson($postData){

        if(isset($postData['firstname']) && isset($postData['lastname'])){
            /*$person = new Person();
            $person->setLastname($postData['lastname']);
            $person->setFirstname($postData['firstname']);
            //*/

            $lastname = cleanString($postData['lastname']);
            $firstname = cleanString($postData['firstname']);
            $req = "INSERT INTO `peoples` (`id`, `firstname`, `lastname`, `image`) VALUES (NULL, :firstname, :lastname , NULL);";
            
            $dataArgs = [
                ":firstname" => $firstname,
                ":lastname" => $lastname,
            ];
            
            databaseInsert($req, $dataArgs);
            
            
        } else {
            // TODO afficher message flash d'erreur
        }
    }
}
