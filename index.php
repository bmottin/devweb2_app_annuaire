<?php

require_once './model/Person.php';
require_once './model/User.php';
require_once './model/NumType.php';
require_once './model/PersonNumber.php';
require_once './tools.php';



require_once './controller/PersonController.php';
require_once './controller/NumTypeController.php';
require_once './controller/PersonNumberController.php';

require_once './controller/database.php';

$pc = new PersonController();

$persons = $pc->getPersons();



$page = "home";

if (isset($_GET['page'])) {
    $page = $_GET['page'];
}

require_once './view/parts/header.php';
switch ($page) {
    case 'home':
        require_once './view/home.php';
        break;
    case 'admin':
        require_once './view/admin.php';
        break;

    case 'action_add_person':
        $pc->addPerson($_POST);
        break;

    default:
        // 404
        require_once './view/404.php';
        break;
}

require_once './view/parts/footer.php';
